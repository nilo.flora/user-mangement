<?php

namespace Fligno\Auth;

use Fligno\StarterKit\Providers\BaseStarterKitServiceProvider;

class AuthServiceProvider extends BaseStarterKitServiceProvider
{
    
    public function register(): void
    {
        parent::register();
        $this->mergeConfigFrom(__DIR__.'/../config/auth.php', 'auth');

        // Register the service the package provides.
        $this->app->singleton('auth', function ($app) {
            return new Auth;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['auth'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/auth.php' => config_path('auth.php'),
        ], 'auth.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/fligno'),
        ], 'auth.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/fligno'),
        ], 'auth.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/fligno'),
        ], 'auth.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
